import React from 'react'
import firebase from 'firebase'
import { clientCredentials } from '../firebaseCredentials'
import InspElement from '../components/InspElement.js'
import AddButton from '../components/AddButton.js'
import Head from '../components/header.js'
import Link from 'next/link'
import Layout from '../components/layout.js'
import {style} from '../components/style.js'

export default class App extends React.Component {

    static async getInitialProps ({req, query}) {
        const user = req && req.session ? req.session.decodedToken : null
        const snap = await req.firebaseServer.database().ref('insps').once('value')
        return { user, inspList: snap.val() }
    }

    constructor(props) {
        super(props)
        this.state = {
            user : this.props.user,
            inspList : this.props.inspList
        };

        this.addDbListener = this.addDbListener.bind(this);

        this.currentCategory = this.props.url.query.category;
    }

    componentDidMount () {
    firebase.initializeApp(clientCredentials)

    if (this.state.user) this.addDbListener()

    firebase.auth().onAuthStateChanged(user => {
      if (user) {
        this.setState({ user: user })
        return user.getToken()
          .then((token) => {
            // eslint-disable-next-line no-undef
            return fetch('/api/login', {
              method: 'POST',
              // eslint-disable-next-line no-undef
              headers: new Headers({ 'Content-Type': 'application/json' }),
              credentials: 'same-origin',
              body: JSON.stringify({ token })
            })
          }).then((res) => this.addDbListener())
      } else {
        this.setState({ user: null })
        // eslint-disable-next-line no-undef
        fetch('/api/logout', {
          method: 'POST',
          credentials: 'same-origin'
        }).then(() => firebase.database().ref('insps').off())
      }
    })
  }

    addDbListener () {
        firebase.database().ref('insps').on('value', snap => {
      
            this.setState({ inspList: snap.val() });
        })
    }

    createInsp(newInspName, categories) {

        const date = new Date().getTime();
        firebase.database().ref('insps/' + date).set({
            id: date,
            title: newInspName,
            categories: categories
        })

        //this.setState({ value: '' })
    }

    handleLogin () {
        firebase.auth().signInWithPopup(new firebase.auth.GoogleAuthProvider());
    }

    handleLogout () {
        firebase.auth().signOut();
    }

    showInspList()
    {   
        if (!this.state.inspList) this.state.inspList = {};

        let inspList = this.state.inspList;          // Object.keys(e.val()).length);  // key, val().Peepijeep

        let keyArray = Object.keys(inspList);

        let inspArray = [];

        for(let i = 0; i < keyArray.length; i++)
        {
            let key = keyArray[i];
            let inspData = inspList[key];

            inspArray.push(inspData);
        }

        if(this.currentCategory)
        {
            inspArray = inspArray.filter((inspData) => {

                console.log(inspData);

                if(inspData.categories[this.currentCategory])
                    return true;
                else
                    return false;
            });
        }

        return inspArray.map((inspData) => {
    
            return(
                <InspElement key={inspData.id} insp={inspData}/>
            );
        });
    }
      

    render () {

        return (
            <div>
                <Head />

                {
                    this.state.user
                    ? <button onClick={this.handleLogout} style={style.logButton}>logout</button>
                    : <button onClick={this.handleLogin} style={style.logButton}>login</button>
                }
                {
                    this.state.user &&
                    <div>
                        <Layout category={this.currentCategory}>
                            <div>
                                <p style={style.create}> create your insp </p>
                            </div>
                            <AddButton createInspFun={this.createInsp.bind(this)} />
                        
                            <div style={style.inspList}>
                                {this.showInspList()}    
                            </div>
                        </Layout>

                    </div>
                }
            </div>
        );
    }

}
   


