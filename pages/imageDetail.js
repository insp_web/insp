import React from 'react'
import firebase from 'firebase'
import { clientCredentials } from '../firebaseCredentials'
import Head from '../components/header.js'
import Link from 'next/link'
import Layout from '../components/layout.js'
import {style} from '../components/style.js'
import SwitchButton from '../components/SwitchButton.js'

export default class imageDetail extends React.Component {

    static async getInitialProps ({req, query}) {
        const user = req && req.session ? req.session.decodedToken : null
        const snap = await req.firebaseServer.database().ref('insps/' + query.inspID + '/images').once('value');
        return { user, images: snap.val(), imageData: snap.val()[query.imageID] }
    }

    constructor(props) {
        super(props)
        this.state = {
            user : this.props.user,
            images : this.props.images,
            imageData : this.props.imageData,
            description : this.props.imageData.description
        };

        this.descriptionChange = this.descriptionChange.bind(this);
        this.inputKeyPress = this.inputKeyPress.bind(this);
        this.descriptionBlur = this.descriptionBlur.bind(this);

        this.addDbListener = this.addDbListener.bind(this);
    }

    componentDidMount () {
    firebase.initializeApp(clientCredentials)

    if (this.state.user) this.addDbListener()

    firebase.auth().onAuthStateChanged(user => {
      if (user) {
        this.setState({ user: user })
        return user.getToken()
          .then((token) => {
            // eslint-disable-next-line no-undef
            return fetch('/api/login', {
              method: 'POST',
              // eslint-disable-next-line no-undef
              headers: new Headers({ 'Content-Type': 'application/json' }),
              credentials: 'same-origin',
              body: JSON.stringify({ token })
            })
          }).then((res) => this.addDbListener())
      } else {
        this.setState({ user: null })
        // eslint-disable-next-line no-undef
        fetch('/api/logout', {
          method: 'POST',
          credentials: 'same-origin'
        }).then(() => firebase.database().ref('insps/' + this.props.url.query.inspID + '/images').off())
      }
    })
  }

    addDbListener () {
        firebase.database().ref('insps/' + this.props.url.query.inspID + '/images').on('value', snap => {
      
            this.setState({ images: snap.val(), imageData: snap.val()[this.props.url.query.imageID] });
        })
    }

    handleLogin () {
        firebase.auth().signInWithPopup(new firebase.auth.GoogleAuthProvider());
    }

    handleLogout () {
        firebase.auth().signOut();
    }

    descriptionChange(event)
    {
        this.setState({description: event.target.value});
    }

    inputKeyPress(event)
    {
        if(event.which == 13)
        {
            event.preventDefault();
            event.target.blur();
        }
    }

    descriptionBlur(event)
    {   
        let newDescription = event.target.value;

        if(this.state.imageData.description != newDescription)
        {   
            firebase.database().ref('insps/' + this.props.url.query.inspID + '/images/' + this.props.url.query.imageID).update({
                description: newDescription
            });
        }
    }

    render () {

        return (
            <div>
                <Head />

                {
                    this.state.user
                    ? <button onClick={this.handleLogout} style={style.logButton}>logout</button>
                    : <button onClick={this.handleLogin} style={style.logButton}>login</button>
                }
                {
                    this.state.user &&
                    <div>
                        <Layout>
                            <div>
                                <p style={style.create}> Image Detail </p>
                            </div>
                            <div style={style.switchButtonDiv}>
                                <SwitchButton type='<' images={this.state.images} inspID={this.props.url.query.inspID} imageID={this.props.url.query.imageID}  />
                                <SwitchButton type='>' images={this.state.images} inspID={this.props.url.query.inspID} imageID={this.props.url.query.imageID}  />
                            </div>
                            
                            <img style={style.singleImage} src={this.props.imageData.image} />
                            
                            <div>
                                <textarea rows="1" type="text" value={this.state.description} onChange={this.descriptionChange} onKeyPress={this.inputKeyPress} onBlur={this.descriptionBlur} style={style.inputStyleDetail}/>
                            </div>

                        </Layout>

                    </div>
                }
            </div>
        );
    }

}
   


