import React from 'react'
import firebase from 'firebase'
import { clientCredentials } from '../firebaseCredentials'
import Head from '../components/header.js'
import Layout from '../components/layout.js'
import {style} from '../components/style.js'
import ImageElement from '../components/ImageElement'
import InspLink from '../components/link'
import CategoryButton from '../components/CategoryButton'


export default class detail extends React.Component {

     static async getInitialProps ({req, query}) {
        const user = req && req.session ? req.session.decodedToken : null
        const snap = await req.firebaseServer.database().ref('insps/' + query.id).once('value')
     
        return { user, insp: snap.val() }
    }

    constructor(props) {
        super(props)
        this.state = {
            user : this.props.user,
            insp : this.props.insp,
            title : this.props.insp.title,
            link: this.props.insp.link,
            activeDrop: false
        };

        this.percent = 0;

        this.titleChange = this.titleChange.bind(this);
        this.inputKeyPress = this.inputKeyPress.bind(this);
        this.titleBlur = this.titleBlur.bind(this);
		this.setCategory = this.setCategory.bind(this);
        this.dragFileEnter = this.dragFileEnter.bind(this);
        this.dragFileLeave = this.dragFileLeave.bind(this);
        this.dropFile = this.dropFile.bind(this);
        this.createLink = this.createLink.bind(this);

        this.showImages = this.showImages.bind(this);
        this.uploadFiles = this.uploadFiles.bind(this);

        this.addDbListener = this.addDbListener.bind(this)
    }

    componentDidMount () {
        firebase.initializeApp(clientCredentials)

        if (this.state.user) this.addDbListener()

        firebase.auth().onAuthStateChanged(user => {
        if (user) {
            this.setState({ user: user })
            return user.getToken()
            .then((token) => {
                // eslint-disable-next-line no-undef
                return fetch('/api/login', {
                method: 'POST',
                // eslint-disable-next-line no-undef
                headers: new Headers({ 'Content-Type': 'application/json' }),
                credentials: 'same-origin',
                body: JSON.stringify({ token })
                })
            }).then((res) => this.addDbListener())
        } else {
            this.setState({ user: null })
            // eslint-disable-next-line no-undef
            fetch('/api/logout', {
            method: 'POST',
            credentials: 'same-origin'
            }).then(() => firebase.database().ref('insps/' + this.props.url.query.id).off())
        }
        })
    }

    addDbListener () {
        firebase.database().ref('insps/' + this.props.url.query.id).on('value', snap => {
            this.setState({ insp: snap.val() });
        })
    }

    handleLogin () {
        firebase.auth().signInWithPopup(new firebase.auth.GoogleAuthProvider());
    }

    handleLogout () {
        firebase.auth().signOut();
    }

    titleChange(event) {
        this.setState({title: event.target.value});
    }

    inputKeyPress(event)
    {
        if(event.which == 13)
        {
            event.preventDefault();
            event.target.blur();
        }
    }

    titleBlur(event)
    {   
        let newTitle = event.target.value;

        if(this.state.insp.title != newTitle)
        {   
            let id = this.state.insp.id;
            firebase.database().ref('insps/' + id).update({
                title: newTitle
            });
        }
    }
	
	setCategory(category, isSelected)
    {
        if (isSelected)
        {
            this.state.insp.categories[category] = true;
        }else{ 

            if (Object.keys(this.state.insp.categories).length == 1)
            {
                return false;
            }

            delete this.state.insp.categories[category];
        }

        let id = this.state.insp.id;
        firebase.database().ref('insps/' + id).update({
            categories: this.state.insp.categories
        });

        return true;
    }
	
    dragFileEnter(event)
    {   
        event.stopPropagation();
        event.preventDefault();
        event.dataTransfer.dropEffect = 'copy'; //none, link, move, copy, Only works in normal Drag Event
        if (!event.dataTransfer.types.includes("Files")) return;

        this.setState({activeDrop: true});
    }

    dragFileLeave(event)
    {
        event.stopPropagation();
        event.preventDefault();

        if (!event.dataTransfer.types.includes("Files")) return;

        this.setState({activeDrop: false});
    }

    dropFile(event)
    {   
        event.stopPropagation();
        event.preventDefault();

        this.setState({activeDrop: false});

        // check if files are dropped

        if (!event.dataTransfer.types.includes("Files")) return;

        this.uploadFiles(event.dataTransfer.files[0]);

    }

    uploadFiles(file)
    {
        let storageRef = firebase.storage().ref('images/' + file.name);
        
        let task = storageRef.put(file);

        let that = this;

        task.on('state_changed', 
            function progress(snapshot){
                this.percent = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
            },
            function error(err){
                this.percent = 0;
            },
            function complete(){
                let inspID = that.state.insp.id;
                //let images = that.state.insp.images;
                //if (!images) images = [];
                //images.push(task.snapshot.downloadURL);
                let imageID = new Date().getTime();

                firebase.database().ref('insps/' + inspID + '/images/' + imageID).set({
                    id: imageID,
                    description: '',
                    image: task.snapshot.downloadURL
                });

                this.percent = 0;
            }

        );
    }

    createLink(newLink) {

        let id = this.state.insp.id;
        firebase.database().ref('insps/' + id).update({
            link: newLink
        });

        this.setState({link: newLink});
    }

    showImages()
    {
        let imageList = this.state.insp.images;
        if (!imageList) imageList = {};
        let key = 0;

        return Object.keys(imageList).map((el) => {
            
            let image = imageList[el];
            key++;

            return(
                <ImageElement key={key} imageData={image} inspID={this.state.insp.id} />
            );
        });
    }

    render () {

        let dropZoneStyle;
        if(this.state.activeDrop){
            dropZoneStyle = style.dropZoneActive
        }else{
            dropZoneStyle = style.dropZone;
        }

        return (
            <div>
                <Head />

                {
                    this.state.user
                    ? <button onClick={this.handleLogout} style={style.logButton}>logout</button>
                    : <button onClick={this.handleLogin} style={style.logButton}>login</button>
                }
                {
                    this.state.user &&
                    <div>
                        <Layout>
                            
                            <input type="text" value={this.state.title} onChange={this.titleChange} onKeyPress={this.inputKeyPress} onBlur={this.titleBlur} style={style.inspInput}/>
                           
                            <InspLink createLink={this.createLink} link={this.state.link}/>

							<div style={style.categoryList}>
                                <CategoryButton text="3d" setCategoryFun={this.setCategory} selected={this.state.insp.categories["3d"]}/>
                                <CategoryButton text="photo" setCategoryFun={this.setCategory} selected={this.state.insp.categories["photo"]}/>
                                <CategoryButton text="ux" setCategoryFun={this.setCategory} selected={this.state.insp.categories["ux"]}/>
                                <CategoryButton text="typography" setCategoryFun={this.setCategory} selected={this.state.insp.categories["typography"]}/>
                                <CategoryButton text="graphic" setCategoryFun={this.setCategory} selected={this.state.insp.categories["graphic"]}/>
                                <CategoryButton text="motion" setCategoryFun={this.setCategory} selected={this.state.insp.categories["motion"]}/>
                                <CategoryButton text="web" setCategoryFun={this.setCategory} selected={this.state.insp.categories["web"]}/>
                                <CategoryButton text="illustration" setCategoryFun={this.setCategory} selected={this.state.insp.categories["illustration"]}/>
                                <CategoryButton text="game" setCategoryFun={this.setCategory} selected={this.state.insp.categories["game"]}/>
                            </div>

                            <div onDragOver={this.dragFileEnter} onDragLeave={this.dragFileLeave} onDrop={this.dropFile} style={dropZoneStyle}>Drop your images here!</div>
                            
                            <div style={style.inspList}>
                                {this.showImages()}
                            </div>                            
                        </Layout>

                    </div>
                }
            </div>
        );
    }

}