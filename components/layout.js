import Link from 'next/link'
import Head from 'next/head'
import {style} from './style.js'
import MenuElement from './MenuElement.js'

export default class ImageElement extends React.Component {

    constructor(props) {
        super(props)
    }

    render() {

        return (
            <div>
                <a href='/' id="inspHome">
                   <img  alt="inspLogo" style={style.logo} src={"../static/insp_logo.svg"}/>
                </a>
            
                <div style={style.flex}>
                    <div id="menu" width="330px" style={style.menu}>
                        <nav>
                            
                            <MenuElement currentCategory={this.props.category} text='3d'/>
                            
                            <MenuElement currentCategory={this.props.category} text='photo'/>

                            <MenuElement currentCategory={this.props.category} text='ux'/>

                            <MenuElement currentCategory={this.props.category} text='typography'/>

                            <MenuElement currentCategory={this.props.category} text='graphic'/>

                            <MenuElement currentCategory={this.props.category} text='motion'/>

                            <MenuElement currentCategory={this.props.category} text='web'/>

                            <MenuElement currentCategory={this.props.category} text='illustration'/>

                            <MenuElement currentCategory={this.props.category} text='game'/>
                         
                        </nav>
                    </div>
                    
                    <div>
                        {this.props.children}
                    </div>
                </div>
	
            </div>
        );
    }

}


