import React from 'react'
import Head from 'next/head'

export default class extends React.Component {

    constructor(props) {
        super(props)
    }

render(){
    return (

  <Head>
    <style>{`
      body { 
        font: bold 20px Gilroy;
        background: #F2F2EA;
        display: flex;
      }
    `}</style>
    {this.props.children}

  </Head>
)
}
}