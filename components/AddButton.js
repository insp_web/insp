import React from 'react'
import {style} from './style.js'
import CategoryButton from './CategoryButton'

export default class AddButton extends React.Component {

    static defaultProps = {
        placeholder: 'create your title'
    };

    constructor(props) {
        super(props)
        this.state = {
            newInsp: 'new Insp'
        }

        this.inputChangeHandler = this.inputChangeHandler.bind(this);
        this.buttonClickHandler = this.buttonClickHandler.bind(this);
        this.setCategory = this.setCategory.bind(this);

        this.categories = {};

    }

    setCategory(category, isSelected)
    {
        if (isSelected)
        {
            this.categories[category] = true;
        }else{ 
            delete this.categories[category];
        }

        return true;
    }

    render() {
        return (
            <div>
                <input type="text" onChange={this.inputChangeHandler} placeholder={this.props.placeholder} style={style.inputStyle}/>
                <div style={style.categoryList}>
                    <CategoryButton text="3d" setCategoryFun={this.setCategory}/>
                    <CategoryButton text="photo" setCategoryFun={this.setCategory}/>
                    <CategoryButton text="ux" setCategoryFun={this.setCategory}/>
                    <CategoryButton text="typography" setCategoryFun={this.setCategory}/>
                    <CategoryButton text="graphic" setCategoryFun={this.setCategory}/>
                    <CategoryButton text="motion" setCategoryFun={this.setCategory}/>
                    <CategoryButton text="web" setCategoryFun={this.setCategory}/>
                    <CategoryButton text="illustration" setCategoryFun={this.setCategory}/>
                    <CategoryButton text="game" setCategoryFun={this.setCategory}/>
                </div>
                <div onClick={this.buttonClickHandler} style={style.addInspButton}>add insp</div>
            </div> 
        );
    }

    inputChangeHandler(event) {
        this.setState({newInsp: event.target.value});
    }

    buttonClickHandler(event) {
        //event.preventDefault();

        if (this.state.newInsp.trim() == "" || Object.keys(this.categories).length == 0) return;

        this.props.createInspFun(this.state.newInsp, this.categories);
    }
}


