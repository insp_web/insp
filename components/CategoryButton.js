import React from 'react'
import {style} from './style.js'

export default class CategoryButton extends React.Component {

    static defaultProps = {
        selected: false
    };

    constructor(props) {
        super(props)
        this.state = {
            mouseEntered: false,
            selected : this.props.selected
        }

        this.mouseEnter = this.mouseEnter.bind(this);
        this.mouseLeave = this.mouseLeave.bind(this);

        this.clickHandler = this.clickHandler.bind(this);
    }

    mouseEnter(event)
    {
        this.setState({mouseEntered: true});
    }

    mouseLeave(event)
    {
        this.setState({mouseEntered: false});
    }

    clickHandler(event)
    {   
        let isSelected = !this.state.selected;

        let bOK = this.props.setCategoryFun(this.props.text, isSelected);

        if (bOK)
        {
            this.setState({selected: isSelected});
        }
    }

    render() {

        let categoryStyle = style.categoryButton;
        /* if (this.state.mouseEntered)
        {
            categoryStyle.backgroundColor = '#e8e5c2';
        } else {
            categoryStyle.backgroundColor = '#F2F2EA';
        } */

        if (this.state.selected)
        {
            categoryStyle.color = '#FF990A';
        }else{
            categoryStyle.color = '#333333';
        }

        return (
            <div onClick={this.clickHandler} onMouseEnter={this.mouseEnter} onMouseLeave={this.mouseLeave} style={categoryStyle}>{this.props.text}</div>
        );
    }

}


