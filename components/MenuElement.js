import React from 'react'
import {style} from './style.js'

export default class MenuElement extends React.Component {


    constructor(props) {
        super(props)
        
    }

    render() {

        let menuStyle;
        if (this.props.currentCategory == this.props.text)
        {
            menuStyle = style.Link_menu_Active;
        } else {
            menuStyle = style.Link_menu;
        }

        return (
            <div>
                <a href={'/index?category=' + this.props.text} style={menuStyle}>{this.props.text}</a>
            </div>
        );
    }
}


