export const style = {
    flex: {
        display: 'flex'
    },
    Hidden: {
        display: 'none'
    },
    positionRelative: {
        position: 'relative',
        top: '0px',
        left: '0px'
    },
    logo: {
        width: '250px',
        marginLeft: '10px'
        
    },
    create: {
            color: '#FF990A',
            fontSize: '45px',
            marginLeft: '530px',
    },

    linkInput:{
        position: 'right',
        width: '815px',
        background: '#D9D9D2',
        color: '#333333',
        border: 'none',
        font: 'bold 15px Gilroy',
        marginLeft: '50px',
        marginBottom: '15px',
        textAlign: 'right',
        height: '20px',
        lineHeight: '20px',
    },

    linkDiv:{
        marginLeft: '50px',
        width: '815px',
        position: 'right',
        textAlign: 'right',
        marginBottom: '30px'
    },

     link: {    
        fontSize: '20px',
        color: '#FF990A',
        textDecoration: 'none'
        //textDecoration: 'none',
    },

    // --- DETAIL PAGE --- 

    inspInput: {                        //Detail Page Title Input
        position: 'right',
        width: '815px',
        height: '60px',
        background: '#F2F2EA',      //D9D9D2
        color: '#333333',
        border: 'none',
        font: 'bold 45px Gilroy',
        marginLeft: '50px',
        marginBottom: '15px',
        textAlign: 'right',
        cursor: 'pointer'
    },

    inputStyle: {                   // create your title & add Link 
        position: 'right',
        width: '815px',
        background: '#D9D9D2',
        color: '#333333',
        border: 'none',
        font: 'bold 25px Gilroy',
        marginLeft: '50px',
        marginBottom: '25px',
        textAlign: 'right',
        height: '35px',
        lineHeight: '35px',
    },

    dropZone: {
        width: "815px",
        height: "75px",
        backgroundColor: "#D9D9D2",
        font: 'bold 25px Gilroy',
        marginTop: '35px',
        marginBottom: '20px',
        marginLeft: '50px',
        textAlign: 'center',
        color: '#707070',       //757575
        lineHeight: '75px'
    },
    dropZoneActive: {
         width: "815px",
        height: "75px",
        backgroundColor: "#FF990A",
        font: 'bold 25px Gilroy',
        marginTop: '35px',
        marginBottom: '20px',
        marginLeft: '50px',
        textAlign: 'center',
        color: '#707070',       //757575
        lineHeight: '75px'
    },
    

        // --- DETAIL PAGE END ---


    inputStyleDetail: {
        position: 'right',
        width: '1000px',
        background: '#D9D9D2',
        color: '#333333',
        border: 'none',
        font: 'bold 25px Gilroy',
        marginLeft: '50px',
        marginBottom: '5px'
    },
    menu:{
        marginLeft: '28px',
        marginTop: '120px',
        display: 'flex'
    },
    Link_menu: {
        font: 'bold 60px Gilroy',
        textDecoration: 'none',
        color: '#333333'
    },
    Link_menu_Active: {
        font: 'bold 60px Gilroy',
        textDecoration: 'none',
        color: '#FF990A'
    },

    addInspButton: {
        cursor: 'Pointer',
        width: '150px',
        height: '45px',
        textAlign: 'center',
        backgroundColor: '#FF990A',
        color: '#F2F2EA',
        font: 'bold 25px Gilroy',
        lineHeight: '45px',
        marginTop: '30px',
        marginBottom: '30px',
        marginLeft: '715px'
        
    },
    inspList: {
        display: 'flex',
        flexWrap: 'wrap',
        width: '1000px',
        marginLeft: '50px',
    },
    inspElement: {
        position: 'relative',
        top: '0px',
        left: '0px',
        width: '200px',
        height: '100px',
        margin: '5px 5px 0px 0px',
        backgroundColor: '#ffffff',
        cursor: 'Pointer'
    },
    inspElementTitle: {
        position: 'absolute',
        /*top: '5px',
        left: '5px',*/
        textAlign: 'center',
        verticalAlign: 'middle',
        lineHeight: '100px',
        display: 'block'
    },
    imageMouseOver:
    {
        opacity: '0.3'
    },
    imageNormal:
    {
        opacity: '1'
    },
    images: {
        width: "200px",
        height: "100px",
        backgroundRepeat: 'no-repeat',
        backgroundPosition: 'center center',
        backgroundSize: 'cover'
    },
    emptyImage: {
        width: "200px",
        height: "100px",
        backgroundColor: "#F2F2EA",
        backgroundImage: 'url("../static/no_image.png")',
        backgroundRepeat: 'no-repeat',
        backgroundPosition: 'center center'
    },
    deleteElement:
    {
        position: 'absolute',
        top: '0px',
        right: '0px',
        width: '30px',
        height: '30px',
        backgroundImage: 'url("../static/cross-remove.png")',
        backgroundRepeat: 'no-repeat',
        backgroundPosition: 'center center',
        zIndex: '1'
    },
    deleteElementActive:
    {
        position: 'absolute',
        top: '0px',
        right: '0px',
        width: '30px',
        height: '30px',
        backgroundImage: 'url("../static/cross-remove_black.png")',
        backgroundRepeat: 'no-repeat',
        backgroundPosition: 'center center',
        zIndex: '1'
    },

    
    categoryList: {
        display: 'flex',
        flexWrap: 'wrap',
        width: '920px',
        marginLeft: '50px'
    },
    
    categoryButton: {
        width: '163px',
        textAlign: 'center',
        cursor: 'pointer',
        color: '#FF990A',
        backgroundColor: '#F2F2EA',
        marginBottom: '5px',
        marginTop: '5px',
        font: 'bold 25px Gilroy',

    },

    switchButtonDiv:{
        marginLeft: '50px',
        position: 'relative',
        width: '1000px',
        height: '50px',
        fontSize: '40px'
    },

    leftButton:{
        position: 'absolute',
        left: '0px',
        top: '0px',
        textDecoration: 'none',
        color: '#FF990A',
    },

    rightButton: {
        position: 'absolute',
        right: '0px',
        top: '0px',
        textDecoration: 'none',
        color: '#FF990A'
    },

    singleImage:{
    marginLeft: '50px',
    width: '1000px',
    height:'auto'
},

    logButton:{
        width: '150px',
        height: '45px',
        font: 'bold 20px Gilroy',
        backgroundColor: '#FF990A',
        marginTop: '10px',
        marginBottom: '20px',
        marginLeft:'1075px',
        color: '#F2F2EA',
        border: 'none',
        lineHeight: '45px',
        cursor: 'pointer'
    },

}