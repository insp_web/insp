import React from 'react'
import {style} from './style.js'
import firebase from 'firebase'
import Link from 'next/link'
import DeleteButton from './DeleteButton'

export default class InspElement extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            mouseEntered: false
        }

        this.mouseEnter = this.mouseEnter.bind(this);
        this.mouseLeave = this.mouseLeave.bind(this);
        this.mouseClick = this.mouseClick.bind(this);

        this.showRecentImage = this.showRecentImage.bind(this);
        this.deleteInsp = this.deleteInsp.bind(this);
    }

    mouseEnter(event)
    {
         this.setState({mouseEntered: true});
    }

    mouseLeave(event)
    {
         this.setState({mouseEntered: false});
    }

    mouseClick(event)
    {   
        event.preventDefault();

        return false;
    }

    deleteInsp()
    {
        let id = this.props.insp.id;
        firebase.database().ref('insps/' + id).remove();
    }

    showRecentImage()
    {   
        let picArray;
        let pictureFound = false;
        
        let imageList = this.props.insp.images;
        if (imageList)
        {
            picArray = Object.keys(imageList);

            if (picArray.length > 0)
            { 
                // pictures found
                pictureFound = true;
            }
        }

        if(pictureFound)
        {
            let lastPicID = picArray[picArray.length -1];
            let lastImage = imageList[lastPicID].image;

            style.images.backgroundImage = 'url(\"' + lastImage + '\")';
           return (<div style={style.images}/> );
        }else{
            return ( <div style={style.emptyImage}/> );
        }
    
    }

    render() {

        let inspTitleStyle;
        let imageOpacityStyle;
        if (this.state.mouseEntered)
        {
            inspTitleStyle = style.inspElementTitle;
            imageOpacityStyle = style.imageMouseOver;
        } else {
            inspTitleStyle = style.Hidden;
            imageOpacityStyle = style.imageNormal;
        }

        return (
            <div>
            <a href={'/detail?id=' + this.props.insp.id}>
                <div onMouseEnter={this.mouseEnter} onMouseLeave={this.mouseLeave} style={style.inspElement}>
                    <div style={inspTitleStyle}>{this.props.insp.title}</div>
                    {
                        this.state.mouseEntered &&
                        <DeleteButton deleteFun={this.deleteInsp} />
                    }
                    
                    <div style={imageOpacityStyle}>
                        {this.showRecentImage()}
                    </div>
                    
                </div>
            </a>
            </div>

        );
    }

}


