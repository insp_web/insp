import React from 'react'
import {style} from './style.js'

export default class DeleteButton extends React.Component {


    constructor(props) {
        super(props)
        this.state = {
            mouseEntered: false
        }

        this.mouseEnter = this.mouseEnter.bind(this);
        this.mouseLeave = this.mouseLeave.bind(this);

        this.buttonClickHandler = this.buttonClickHandler.bind(this);

    }

    mouseEnter(event)
    {
        this.setState({mouseEntered: true});
    }

    mouseLeave(event)
    {
        this.setState({mouseEntered: false});
    }

    inputChangeHandler(event) {
        this.setState({newInsp: event.target.value});
    }

    buttonClickHandler(event) {
        
        event.stopPropagation();
        event.preventDefault();
   
        this.props.deleteFun();
    }

    render() {

        let deleteInspStyle;
        if (this.state.mouseEntered)
        {
            deleteInspStyle = style.deleteElementActive;
        } else {
            deleteInspStyle = style.deleteElement;
        }

        return (
            <div onClick={this.buttonClickHandler} onMouseEnter={this.mouseEnter} onMouseLeave={this.mouseLeave} style={deleteInspStyle} />
        );
    }
}


