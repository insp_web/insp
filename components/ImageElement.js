import React from 'react'
import {style} from './style.js'
import firebase from 'firebase'
import Link from 'next/link'
import DeleteButton from './DeleteButton'

export default class ImageElement extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            mouseEntered: false
        }

        this.mouseEnter = this.mouseEnter.bind(this);
        this.mouseLeave = this.mouseLeave.bind(this);
        this.mouseClick = this.mouseClick.bind(this);

        this.deleteImage = this.deleteImage.bind(this);
    }

    mouseEnter(event)
    {
         this.setState({mouseEntered: true});
    }

    mouseLeave(event)
    {
         this.setState({mouseEntered: false});
    }

    mouseClick(event)
    {   
        event.preventDefault();

        return false;
    }

    deleteImage()
    {
        let id = this.props.imageData.id;
        firebase.database().ref('insps/' + this.props.inspID + '/images/' + id).remove();
    }

    render() {

        let inspTitleStyle;
        let imageOpacityStyle;
        if (this.state.mouseEntered)
        {
            inspTitleStyle = style.inspElementTitle;
            imageOpacityStyle = style.imageMouseOver;
        } else {
            inspTitleStyle = style.Hidden;
            imageOpacityStyle = style.imageNormal;
        }

        style.images.backgroundImage = 'url(\"' + this.props.imageData.image + '\")';

        return (
        <div>
            <a href={'/imageDetail?inspID=' + this.props.inspID + '&imageID=' + this.props.imageData.id}>
                <div onMouseEnter={this.mouseEnter} onMouseLeave={this.mouseLeave} style={style.inspElement}>
                    <div style={inspTitleStyle}>{this.props.imageData.title}</div>
                    {
                        this.state.mouseEntered &&
                        <DeleteButton deleteFun={this.deleteImage} />
                    }
                    
                    <div style={imageOpacityStyle}>
                        <div style={style.images}/>
                    </div>
                    
                </div>
            </a>
        </div>
        );
    }

}


