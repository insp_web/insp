import React from 'react'
import {style} from './style.js'
import firebase from 'firebase'
import Link from 'next/link'

export default class link extends React.Component {

static defaultProps = {
        placeholder: 'add a link'
    };

    constructor(props) {
        super(props)
        this.state = {
            userLink: this.props.link,
            shownLink : this.props.link
        }

        this.linkChange = this.linkChange.bind(this);
        this.linkKeyPress = this.linkKeyPress.bind(this);
        this.linkBlur = this.linkBlur.bind(this);

    }

    linkChange(event) {
        this.setState({userLink: event.target.value});
    }

    linkKeyPress(event)
    {
        if(event.which == 13)
        {
            event.preventDefault();
            event.target.blur();
        }
    }

    linkBlur(event)
    {   
        this.state.shownLink = this.state.userLink;
        this.props.createLink(this.state.shownLink);
    }

    render() {
            return (
            <div>
                <input style={style.linkInput} type="text" placeholder={this.props.placeholder} value={this.state.userLink} onChange={this.linkChange} onKeyPress={this.linkKeyPress} onBlur={this.linkBlur} ref="createInput"/>
                
                <div style={style.linkDiv}>
                <a href={this.state.shownLink} target="_blank" style={style.link} >{this.state.shownLink}</a>
                </div>
            </div>
            );
        }

        

}



    