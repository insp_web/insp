import React from 'react'
import {style} from './style.js'

export default class SwitchButton extends React.Component {

    //static defaultProps = {};

    constructor(props) {
        super(props)
        this.state = {
             mouseEntered: false
        }
        this.mouseEnter = this.mouseEnter.bind(this);
        this.mouseLeave = this.mouseLeave.bind(this);
      
    }

    mouseEnter(event)
    {
         this.setState({mouseEntered: true});
    }

    mouseLeave(event)
    {
         this.setState({mouseEntered: false});
    }

    getImageID()
    {
        let imageIDs = Object.keys(this.props.images);
        let index = -1;

        for(let i = 0; i < imageIDs.length; i++)
        {
            if(this.props.imageID == imageIDs[i])
            {
                if(this.props.type == '<')
                {
                    index = i - 1;
                } else
                {
                    index = i + 1;
                }
            }
        }

        if (index >= 0 && index < imageIDs.length)
        {
            return imageIDs[index];
        }else{
            return -1;
        }
    }

    render() {
        let switchButtonStyle;
        if (this.props.type == '<')
        {
            switchButtonStyle = style.leftButton;
        } else {
            switchButtonStyle = style.rightButton;
        }

        let imageToShow = this.getImageID();
        
        let showButton;
        let url;

        if (imageToShow != -1) {
            url = '/imageDetail?inspID=' + this.props.inspID + '&imageID=' + imageToShow;
            showButton = true;
        }else{
            showButton = false;
        }

        return (
            
            <div>
                {
                    showButton &&
                    <a href={url} style={switchButtonStyle} >
                        <div onMouseEnter={this.mouseEnter} onMouseLeave={this.mouseLeave} >{this.props.type}</div>
                    </a> 
                }
            </div>
        );
    }
}

